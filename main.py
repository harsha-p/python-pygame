"""boat game"""
from config import *

clock = pygame.time.get_ticks()

pygame.display.set_caption("Boat race")
icon = pygame.image.load('icon.png')
pygame.display.set_icon(icon)
playerImg = pygame.image.load('ship.png')
Left = 600
Right = 400
num_enemy = 22
enemyImg = []
enemyX = []
enemyY = []
bool11 = [False for i in range(num_enemy)]
bool12 = [False for j in range(num_enemy)]
bool21 = [False for k in range(num_enemy)]
bool22 = [False for z in range(num_enemy)]


def player(x, y):
    """make player"""
    screen.blit(playerImg, (x, y))


def enemy(x, y, i):
    """make enemy"""
    screen.blit(enemyImg[i], (x, y))


def show_score(level, p, value11, value12, value21, value22, time11, time12, time21, start_time):
    """to show score ,level and player details on top of screen"""
    if level == 1:
        if p == 1:
            value = font.render("Level1 Player1" + " Score:" + str(value11) + " Time: " +
                                str(int(time.time() - start_time)), True, (255, 140, 0))
        else:
            value = font.render("Level1 Player2" + " Score:" + str(value12) + " Time: " +
                                str(int(time.time() - start_time - time11 - 2)), True, (255, 140, 0))
    else:
        if p == 1:
            value = font.render("Level2 Player1" +
                                " Score:" + str(value21) + " Time: " +
                                str(int(time.time() - start_time - time11 - time12 - 4)), True, (255, 140, 0))
        else:
            value = font.render("Level2 Player2" + " Score:" + str(value22) + " Time: " +
                                str(int(time.time() - start_time - time11 - time12 - time21 - 6)), True, (255, 140, 0))
    screen.blit(value, (0, 0))


for i in range(num_enemy):
    if i <= 9:
        enemyImg.append(pygame.image.load('submarine.png'))
    else:
        enemyImg.append(pygame.image.load('rock.png'))
enemyY.append(2*width//10 + 30)
enemyY.append(2*width//10 + 30)
enemyY.append(3*width//10 + 50)
enemyY.append(3*width//10 + 50)
enemyY.append(4*width//10 + 70)
enemyY.append(4*width//10 + 70)
enemyY.append(5*width//10 + 90)
enemyY.append(5*width//10 + 90)
enemyY.append(6*width//10 + 110)
enemyY.append(6*width//10 + 110)

enemyX.append(400)
enemyX.append(1100)
enemyX.append(200)
enemyX.append(900)
enemyX.append(200)
enemyX.append(759)
enemyX.append(100)
enemyX.append(937)
enemyX.append(1100)
enemyX.append(572)

enemyY.append(2*width//10 - 15)
enemyY.append(2*width//10 - 15)
enemyY.append(3*width//10 + 5)
enemyY.append(3*width//10 + 5)
enemyY.append(4*width//10 + 25)
enemyY.append(4*width//10 + 25)
enemyY.append(5*width//10 + 45)
enemyY.append(5*width//10 + 45)
enemyY.append(6*width//10 + 65)
enemyY.append(6*width//10 + 65)
enemyY.append(7*width//10 + 85)
enemyY.append(7*width//10 + 85)

enemyX.append(234)
enemyX.append(861)
enemyX.append(59)
enemyX.append(1275)
enemyX.append(987)
enemyX.append(546)
enemyX.append(156)
enemyX.append(725)
enemyX.append(34)
enemyX.append(1050)
enemyX.append(450)
enemyX.append(980)


def border_image(l1, l2, l3, l4):
    """to make image be on screen"""
    l1 += l3
    if l1 <= 10:
        l1 = 10
    if l1 >= 1300:
        l1 = 1300
    l2 += l4
    if l2 <= 10:
        l2 = 10
    if l2 >= 640:
        l2 = 640
    return l1, l2


def score11(lol, py):
    """find score of player 1 in level 1"""
    for i in range(num_enemy):
        if py <= enemyY[i]:
            if not bool11[i]:
                bool11[i] = True
                if i <= 3:
                    lol += 10
                else:
                    lol += 5
    return lol


def score12(lol, py):
    """find score of player 2 in level 1"""
    for i in range(num_enemy):
        if py >= enemyY[i]:
            if not bool12[i]:
                bool12[i] = True
                if i <= 3:
                    lol += 10
                else:
                    lol += 5
    return lol


def score21(lol, py):
    """find score of player 2 in level 1"""
    for i in range(num_enemy):
        if py <= enemyY[i]:
            if not bool21[i]:
                bool21[i] = True
                if i <= 3:
                    lol += 10
                else:
                    lol += 5
    return lol


def score22(lol, py):
    """find score of player 2 in level 2"""
    for i in range(num_enemy):
        if py >= enemyY[i]:
            if not bool22[i]:
                bool22[i] = True
                if i <= 3:
                    lol += 10
                else:
                    lol += 5
    return lol


def game_loop():
    """game loop to run game"""
    start_time = time.time()
    time11 = 0
    time12 = 0
    time21 = 0
    time22 = 0
    px = 10
    py = 640
    px_change = 0
    py_change = 0
    value11 = 0
    value12 = 0
    value21 = 0
    value22 = 0
    level = 1
    p = 1
    game_over = False
    running = True
    while running:
        while game_over:
            time1 = time11 + time22
            time2 = time12 + time22
            score1 = value11 + value21
            score2 = value12 + value22
            score1 /= time1
            score2 /= time2
            screen.fill((0, 128, 255))
            over = font.render("Game Over", True, (255, 0, 0))
            if score1 > score2:
                win = font.render("Player1 is winner", True, (255, 0, 0))
            elif score1 < score2:
                win = font.render("Player2 is winner", True, (255, 0, 0))
            else:
                win = font.render("Both Player1 and Player2 are winners", True, (255, 0, 0))
            options = font.render("Type R to replay or Q to quit", True, (255, 0, 0))
            screen.blit(win, (400, 250))
            screen.blit(over, (400, 200))
            screen.blit(options, (400, 300))
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        running = False
                        pygame.quit()
                    elif event.key == pygame.K_r:
                        for i in range(num_enemy):
                            bool11[i] = False
                            bool12[i] = False
                            bool21[i] = False
                            bool22[i] = False
                        game_loop()
                        return

        screen.fill((100, 150, 234))
        pygame.draw.rect(screen, (32, 32, 32), (0, 0, length, 40))
        pygame.draw.rect(screen, (153, 76, 0), (0, 2*width//10 - 20, length, 40))
        pygame.draw.rect(screen, (153, 76, 0), (0, 3*width//10, length, 40))
        pygame.draw.rect(screen, (153, 76, 0), (0, 4*width//10 + 20, length, 40))
        pygame.draw.rect(screen, (153, 76, 0), (0, 5*width//10 + 40, length, 40))
        pygame.draw.rect(screen, (153, 76, 0), (0, 6*width//10 + 60, length, 40))
        pygame.draw.rect(screen, (153, 76, 0), (0, 7*width//10 + 80, length, 40))
        show_score(level, p, value11, value12, value21, value22, time11, time12, time21, start_time)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    px_change = -1
                if event.key == pygame.K_RIGHT:
                    px_change = 1
                if event.key == pygame.K_UP:
                    py_change = -1
                if event.key == pygame.K_DOWN:
                    py_change = 1
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT:
                    px_change = 0
                    py_change = 0
                if event.key == pygame.K_LEFT:
                    px_change = 0
                    py_change = 0
                if event.key == pygame.K_UP:
                    px_change = 0
                    py_change = 0
                if event.key == pygame.K_DOWN:
                    px_change = 0
                    py_change = 0
        px, py = border_image(px, py, px_change, py_change)
        if level == 1:
            if p == 1:
                value11 = score11(value11, py)
            else:
                value12 = score12(value12, py)
        else:
            if p == 1:
                value21 = score21(value21, py)
            else:
                value22 = score22(value22, py)
        for i in range(num_enemy):
            if i <= 9:
                enemyX[i] += level
                if enemyX[i] >= 1300:
                    enemyX[i] = 10
            collision = is_collision(enemyX[i], enemyY[i], px, py)
            if collision:
                if p == 1:
                    if level == 1:
                        message_display('Collision took place')
                        message_display('Player2 turn in level1')
                        time11 = time.time() - start_time - 2
                    else:
                        message_display('Collision took place')
                        message_display('Player2 turn in level2')
                        time21 = time.time() - start_time - time11 - time12 - 6
                    p = 2
                    px = 10
                    py = 40
                elif p == 2:
                    if level == 1:
                        message_display('Collision took place')
                        message_display('Player1 turn in level2')
                        time12 = time.time() - start_time - time11 - 4
                        p = 1
                        px = 10
                        py = 640
                        level = 2
                    else:
                        message_display('Collision took place')
                        time22 = time.time() - start_time - time11 - time12 - time21 - 8
                        game_over = True
            if py == 40:
                if p == 1:
                    if level == 1:
                        message_display('Success')
                        message_display('Player2 turn in level1')
                        time11 = time.time() - start_time - 2
                    else:
                        message_display('Success')
                        message_display('Player2 turn in level2')
                        time21 = time.time() - start_time - time11 - time12 - 6
                    p = 2
                    px = 10
                    py = 40
            elif py == 640:
                if p == 2:
                    if level == 1:
                        message_display('Success')
                        message_display('Player1 turn in level2')
                        time12 = time.time() - start_time - time11 - 4
                        level = 2
                        p = 1
                        px = 10
                        py = 640
                    else:
                        time22 = time.time() - start_time - time11 - time12 - time21 - 8
                        game_over = True
            enemy(enemyX[i], enemyY[i], i)
            player(px, py)
        pygame.display.update()


game_loop()
pygame.quit()
quit()
