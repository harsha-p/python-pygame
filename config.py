"""config file"""
import pygame
import math
import time
pygame.init()
length = 1366
width = 700
screen = pygame.display.set_mode((length, width))
font = pygame.font.Font('freesansbold.ttf', 32)


def is_collision(x1, y1, x2, y2):
    """to check if collision takes place"""
    distance = math.sqrt((math.pow(x1 - x2, 2)) + (math.pow(y1 - y2, 2)))
    if distance <= 25:
        return True
    else:
        return False


def message_display(text):
    """to display message"""
    screen.fill((51, 153, 255))
    surface = font.render(text, True, (204, 0, 102,))
    screen.blit(surface, (length//2 - 150, width//2 - 50))
    pygame.display.update()
    time.sleep(1)
